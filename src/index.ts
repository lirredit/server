import "reflect-metadata";
import "dotenv-safe/config";
import {ApolloServer} from "apollo-server-express";
import express from "express";
import {buildSchema} from "type-graphql";
import {HelloResolver} from "./resolvers/hello";
import {ApolloServerPluginLandingPageGraphQLPlayground} from "apollo-server-core";
import {PostResolver} from "./resolvers/post";
import {UserResolver} from "./resolvers/user";
import session from "express-session";
import connectRedis from "connect-redis";
import Redis from "ioredis";
import {__prod__, COOKIE_NAME} from "./constants";
import cors from "cors";
import {createConnection} from "typeorm";
import {Post} from "./entities/Post";
import {User} from "./entities/User";
import {Updoot} from "./entities/Updoot";
import path from "path";
import {createUserLoader} from "./utils/createUserLoader";
import {createUpdootLoader} from "./utils/createUpdootLoader";


const main = async () => {
  const conn = await createConnection({
    type: "postgres",
    url: process.env.DATABASE_URL,
    logging: true,
    //synchronize: true,
    migrations: [path.join(__dirname, "./migrations/*")],
    entities: [Post, User, Updoot]
  })
  conn.runMigrations();

  //await Post.delete({})

  const app = express();

  const RedisStore = connectRedis(session);
  const redis = new Redis(process.env.REDIS_URL);

  redis.on("connect", function () {
    console.log("Redis connected");
  });

  redis.on("error", (err) => {
    console.log(err);
  });

  app.use(
    session({
      name: COOKIE_NAME,
      store: new RedisStore({
        client: redis,
        disableTouch: true,
      }),
      cookie: {
        maxAge: 1000 * 60 * 60 * 24 * 90,
        sameSite: "lax",
        httpOnly: true,
        secure: __prod__,
        domain: __prod__ ? ".codeponder.com" : undefined,
      },
      saveUninitialized: false,
      secret: process.env.SESSION_SECRET as string,
      resave: false,
    })
  );
app.set("proxy", 1)
  app.use(
    cors({
      origin: process.env.CORS_ORIGIN,
      credentials: true,
    })
  );
  const apolloServer = new ApolloServer({
    plugins: [ApolloServerPluginLandingPageGraphQLPlayground()],
    schema: await buildSchema({
      resolvers: [HelloResolver, PostResolver, UserResolver],
      validate: false,
    }),
    context: ({req, res}) => ({
      req,
      res,
      redis,
      userLoader: createUserLoader(),
      updootLoader: createUpdootLoader()
    }),
  });

  await apolloServer.start();

  apolloServer.applyMiddleware({
    app,
    cors: false
  });

  await app.listen(parseInt(process.env.PORT), () => {
    console.log("Server started on localhost:3015");
  });
};

main().catch((err) => {
  console.error(err);
});
