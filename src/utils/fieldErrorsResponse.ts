interface FieldError {
  field: string
  message: string
}

export const fieldErrorsResponse = (...errors: FieldError[]) => {
  return {
    errors
  }
}