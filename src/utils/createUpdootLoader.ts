import DataLoader from "dataloader";
import {Updoot} from "../entities/Updoot";

interface UpdootReqIds {
  postId: number,
  userId: number
}

export const createUpdootLoader = () =>
  new DataLoader<UpdootReqIds, Updoot | null>(
    async (keys) => {
      const updoots = await Updoot.findByIds(keys as UpdootReqIds[])
      const updootIdsToUpdoot: Record<string, Updoot> = {}
      updoots.forEach((u) => {
        updootIdsToUpdoot[`${u.userId}|${u.postId}`] = u
      })
      return keys.map((key) => updootIdsToUpdoot[`${key.userId}|${key.postId}`])
    })