import {UsernamePasswordInputs} from "../resolvers/usernamePasswordInputs";

export const validateRegister = ({username, email, password}: UsernamePasswordInputs) => {
  if (username.length <= 2) {
    return [{
      field: "username",
      message: "username is too short"
    }]
  }
  if (!email.includes("@")) {
    return [{
      field: "email",
      message: "invalid email"
    }]
  }
  if (username.includes("@")) {
    return [{
      field: "username",
      message: "cannot include an @"
    }]
  }
  if (password.length <= 3) {
    return [{
      field: "password",
      message: "password is too short"
    }]
  }

  return null
}