import {Arg, Ctx, Field, FieldResolver, Mutation, ObjectType, Query, Resolver, Root} from "type-graphql";
import {MyContext} from "../types";
import {User} from "../entities/User";
import argon2 from "argon2";
import {COOKIE_NAME, FORGET_PASSWORD_PREFIX} from "../constants";
import {UsernamePasswordInputs} from "./usernamePasswordInputs";
import {validateRegister} from "../utils/validateRegister";
import {sendEmail} from "../utils/sendEmail";
import {v4} from "uuid";
import {fieldErrorsResponse} from "../utils/fieldErrorsResponse";
import {getConnection} from "typeorm";

@ObjectType()
class FieldError {
  @Field()
  field: string
  @Field()
  message: string
}

@ObjectType()
class UserResponse {
  @Field(() => [FieldError], {nullable: true})
  errors?: FieldError[]

  @Field(() => User, {nullable: true})
  user?: User
}

@Resolver(User)
export class UserResolver {
  @FieldResolver(() => String)
  email(@Root() user: User, @Ctx() {req}: MyContext) {
    if (req.session.userId === user.id) {
      return user.email
    }
    return "";
  }

  @Mutation(() => UserResponse)
  async changePassword(
    @Arg("token") token: string,
    @Arg("newPassword") newPassword: string,
    @Ctx() {redis, req}: MyContext
  ): Promise<UserResponse> {
    if (newPassword.length <= 3) {
      return fieldErrorsResponse({
        field: "newPassword",
        message: "password is too short"
      })
    }

    const key = FORGET_PASSWORD_PREFIX + token
    const userId = await redis.get(key)
    if (!userId) {
      return fieldErrorsResponse({
        field: "token",
        message: "token expired"
      })
    }
    const userIdNum = parseInt(userId)
    const user = await User.findOneBy({id: userIdNum})

    if (!user) {
      return fieldErrorsResponse({
        field: "token",
        message: "user no longer exists"
      })
    }
    await User.update({id: userIdNum}, {password: await argon2.hash(newPassword)})

    await redis.del(key)

    req.session.userId = user.id

    return {user}
  }

  @Mutation(() => Boolean)
  async forgotPassword(
    @Arg("email") email: string,
    @Ctx() {redis}: MyContext
  ) {
    const user = await User.findOneBy({email})
    if (!user) {
      return true
    }

    const token = v4()
    await redis.set(
      FORGET_PASSWORD_PREFIX + token,
      user.id,
      'EX',
      1000 * 60 * 60 * 24 * 3) //3 days
    await sendEmail(email,
      `<a href="http://localhost:3000/change-password/${token}">reset password</a>`
    )
    return true
  }


  @Query(() => User, {nullable: true})
  async me(
    @Ctx() {req}: MyContext
  ) {
    if (!req.session.userId) {
      return null
    }
    return User.findOneBy({id: req.session.userId})
  }

  @Mutation(() => UserResponse)
  async register(
    @Arg("options") {username, password, email}: UsernamePasswordInputs,
    @Ctx() {req}: MyContext
  ): Promise<UserResponse> {
    const errors = validateRegister({username, password, email})
    if (errors) {
      return {errors}
    }
    const hashedPassword = await argon2.hash(password)
    let user;
    try {
      const result = await getConnection()
        .createQueryBuilder()
        .insert()
        .into(User)
        .values({
          username,
          email,
          password: hashedPassword,
        })
        .returning("*")
        .execute()
      user = result.raw[0];
    } catch (err) {
      if (err.code === "23505") {
        return {
          errors: [{
            field: "username",
            message: "username already taken"
          }]
        }
      }
    }

    req.session.userId = user.id;

    return {user}
  }

  @Mutation(() => UserResponse)
  async login(
    @Arg("usernameOrEmail") usernameOrEmail: string,
    @Arg("password") password: string,
    @Ctx() {req}: MyContext
  ): Promise<UserResponse> {
    const user = await User.findOneBy(
      usernameOrEmail.includes("@")
        ? {email: usernameOrEmail}
        : {username: usernameOrEmail})
    if (!user) {
      return {
        errors: [{
          field: 'usernameOrEmail',
          message: "this username doesn't exist"
        }]
      }
    }
    const valid = await argon2.verify(user.password, password)
    if (!valid) {
      return {
        errors: [{
          field: 'password',
          message: "incorrect password"
        }]
      }
    }

    req.session.userId = user.id;
    return {user}
  }

  @Mutation(() => Boolean)
  logout(
    @Ctx() {req, res}: MyContext
  ) {
    return new Promise(resolve => req.session.destroy(err => {
      res.clearCookie(COOKIE_NAME)
      if (err) {
        console.log(err)
        resolve(false)
        return
      }
      resolve(true)
    }))
  }
}